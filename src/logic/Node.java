package logic;

import java.util.ArrayList;

/**
 * Created by Bohdan on 20.06.2015.
 */
class Node {

    String text;

    String name;

    ArrayList<Node> nodes;

    public Node(){
        this.nodes = new ArrayList<>();
    }

    public Node(String name){
        this();
        this.name = name;
    }

    public Node(String name, String text){
        this(name);
        this.text = text;
    }

    public String toString(){
        if (nodes.size() == 0) return name +" = "+text;
        String s = "\n"+name+":";
        s+=nodes.toString();
        return s;
    }
}
