package logic;

import java.io.*;

/**
 * Created by Bohdan on 20.06.2015.
 */
public class Starter {

    private static String input(){
        String xml = "";
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("test.xml")))) {
            String tmp = reader.readLine();
            while (tmp!= null){
                xml += tmp.trim();
                tmp = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return xml;
    }

    private static void output(String text){
       try(BufferedWriter writer = new BufferedWriter(new FileWriter("output.txt"))) {
           writer.write(text);
       } catch (IOException e) {
           e.printStackTrace();
       }
    }

    public static void main(String[] args) {
        String xml = input();
        Tree treeMaker = new Tree(xml);
        Node root = treeMaker.tree;
        System.out.println(root);
        output(root.toString());
    }
}
