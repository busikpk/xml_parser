package logic;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Bohdan on 20.06.2015.
 */
class Tree {

    private ArrayList<String> tags;

    Node tree;

    private String xml;

    public Tree(String xml){
        this.tags = tags(xml);
        this.xml = xml;
        makeTree();
    }

    private ArrayList<String> tags(String input){
        ArrayList<String> tags = new ArrayList<>();
        Pattern regxTag = Pattern.compile("<([^<>]+)>");
        Matcher matcherTag = regxTag.matcher(input);
        while (matcherTag.find()) {
            String tmp = matcherTag.group();
            tags.add(tmp);
        }
        return tags;
    }

    private String getTagContent(String tag, String input){
        Pattern regexInTag = Pattern.compile(tag+">"+"(.*?)"+"</"+tag);
        Matcher matcher = regexInTag.matcher(input);
        while (matcher.find()) return matcher.group(1);
        return null;
    }

    private void makeTree(){
        String rootTag = tags.get(0);
        tree = new Node(rootTag.substring(1,rootTag.length()-1));
        recTree(tree);
    }

    private int index ;

    private Node recTree(Node root){
        if(index + 2 >= tags.size()) return root;
        String tmp1 = tags.get(++index);
        String tmp2 = tags.get(index + 1);
        if (tmp1.substring(2, tmp1.length()-1).equals(root.name)) return root;
        if (tmp1.substring(1, tmp1.length() - 1).equals(tmp2.substring(2, tmp2.length() - 1))){
            Node leafe = new Node(tmp1.substring(1, tmp1.length()-1));
            leafe.text = getTagContent(tmp1.substring(1, tmp1.length()-1),xml);
            xml = xml.substring(xml.indexOf(tmp1));
            root.nodes.add(leafe);
            index++;
        }else {
            if (!tmp1.startsWith("</")){
                Node newRoot = new Node(tmp1.substring(1,tmp1.length()-1));
                root.nodes.add(newRoot);
                recTree(newRoot);
            }
        }
        return recTree(root);
    }
}
